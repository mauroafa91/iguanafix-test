package co.moma.iguanafix.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import co.moma.iguanafix.R
import co.moma.iguanafix.adapters.AddressAdapter
import co.moma.iguanafix.adapters.ContactListAdapter
import co.moma.iguanafix.adapters.PhoneNumberAdapter
import co.moma.iguanafix.interfaces.IContactInterface
import co.moma.iguanafix.model.Contact
import co.moma.iguanafix.model.RetrofitClient
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_contact_details.*
import kotlinx.android.synthetic.main.vertical_rv.*

class ContactDetails : AppCompatActivity() {

    internal lateinit var contactAPI: IContactInterface
    internal lateinit var compositeDisposable: CompositeDisposable
    private var iUserID: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_details)

        if(intent.extras != null) {
            iUserID = intent.getIntExtra("user_id", 0)
        }

        val retroFit = RetrofitClient.instance
        contactAPI = retroFit.create(IContactInterface::class.java)
        compositeDisposable = CompositeDisposable()

        compositeDisposable.add(contactAPI.getContact(iUserID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{contact->displayData(contact)}
        )
    }

    fun displayData(contact: Contact) {
        tv_contact_full_name.text = String.format("%s %s", contact.first_name, contact.last_name)
        tv_contact_birth_date.text = contact.birth_date

        var date = contact.created_at.substringBefore("T")
        var time = contact.created_at.substringAfter("T")

        tv_creation_date.text = String.format("%s at %s", date, time.substring(0, time.length - 1))

        rv_phone_number.setHasFixedSize(true)
        rv_phone_number.layoutManager = LinearLayoutManager(this)
        rv_phone_number.adapter = PhoneNumberAdapter(contact.phones)

        rv_addresses.setHasFixedSize(true)
        rv_addresses.layoutManager = LinearLayoutManager(this)
        rv_addresses.adapter = AddressAdapter(contact.addresses)

        Glide.with(iv_contact_image.context).load(contact.photo).into(iv_contact_image)
    }
}
