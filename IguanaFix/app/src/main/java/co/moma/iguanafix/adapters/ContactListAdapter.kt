package co.moma.iguanafix.adapters

import android.content.Intent
import android.graphics.Picture
import android.support.v7.util.DiffUtil
import android.support.v7.view.menu.ActionMenuItemView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.moma.iguanafix.Activities.ContactDetails
import co.moma.iguanafix.R
import co.moma.iguanafix.model.Contact
import com.bumptech.glide.Glide
import android.graphics.drawable.PictureDrawable
import android.support.v4.view.ViewCompat.animate
import co.moma.iguanafix.Utils.GlideApp
import co.moma.iguanafix.Utils.SvgSoftwareLayerSetter
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider


class ContactListAdapter(private var contactList: ArrayList<Contact>) : RecyclerView.Adapter<ContactListAdapter.ViewHolder>(), SectionTitleProvider {

    private var TAG = "ContactListAdapter"
    private var bFiltering = false
    private var filteredContactList = ArrayList<Contact>()
    private var requestBuilder: RequestBuilder<PictureDrawable>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        requestBuilder = GlideApp.with(parent.context)
                .`as`(PictureDrawable::class.java)
                .error(R.drawable.ic_account_circle_black_48px)
                .transition(withCrossFade())
                .listener(SvgSoftwareLayerSetter())

        val v = LayoutInflater.from(parent.context).inflate(R.layout.activity_main, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        when(bFiltering){
            true -> return filteredContactList.size
            false -> return contactList.size
        }
    }

    override fun getSectionTitle(position: Int): String {
        return contactList[position].first_name.substring(0, 1)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var currentContactList = ArrayList<Contact>()

        when(bFiltering){
            true -> currentContactList = filteredContactList
            false -> currentContactList = contactList
        }
        //Set the image with Glide using the URI
        if(!currentContactList[position].thumb.contains(".png")) {
            requestBuilder!!.load(currentContactList[position].thumb).into(holder.ivContactThumbnail)
        }
        else {
            Glide.with(holder.itemView.context).load(currentContactList[position].thumb).into(holder.ivContactThumbnail)
        }

        holder.tvContactName.text = String.format("%s %s", currentContactList[position].first_name, currentContactList[position].last_name)
        holder.tvContactDefaultPhoneNumber.text = currentContactList[position].phones[0].number

        holder.itemView.setOnClickListener{
            try {
                val intent = Intent(holder.itemView.context, ContactDetails::class.java)
                intent.putExtra("user_id", currentContactList[position].user_id)
                holder.itemView.context.startActivity(intent)
            }
            catch(ex: Exception) {
                Log.e(TAG, ex.message.toString())
            }
        }
    }

    fun filterContacts(query: String){
        bFiltering = true

        val newContactList = contactList.filter{ contact -> contact.first_name.contains(query, true)
        || contact.last_name.contains(query, true)} as ArrayList<Contact>

        if(newContactList.size == contactList.size) {
            DiffUtil.calculateDiff(ContactDiffCallback(newContactList, filteredContactList), false).dispatchUpdatesTo(this)
        }
        else {
            DiffUtil.calculateDiff(ContactDiffCallback(newContactList, contactList), false).dispatchUpdatesTo(this)
        }
        filteredContactList = newContactList
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivContactThumbnail = itemView.findViewById<ImageView>(R.id.iv_contact_thumb)
        val tvContactName = itemView.findViewById<TextView>(R.id.tv_contact_name)
        val tvContactDefaultPhoneNumber = itemView.findViewById<TextView>(R.id.tv_contact_default_number)
    }

    class ContactDiffCallback(private val newRows : List<Contact>, private val oldRows : List<Contact>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }

        override fun getOldListSize(): Int = oldRows.size

        override fun getNewListSize(): Int = newRows.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }
    }
}