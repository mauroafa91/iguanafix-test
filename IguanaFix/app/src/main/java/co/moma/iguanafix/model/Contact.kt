package co.moma.iguanafix.model

import java.io.Serializable

class Contact: Serializable {
    var user_id: Int = 0
    var created_at: String = ""
    var birth_date: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var phones: ArrayList<PhoneNumber> = ArrayList()
    var thumb: String = ""
    var photo: String = ""
    var addresses: ArrayList<Address> = ArrayList()

    constructor()

    constructor(i_user_id: Int, i_created_at: String, i_birth_date: String, i_first_name: String,
                i_last_name: String, i_phones: ArrayList<PhoneNumber>, i_thumb: String,
                i_photo: String, i_addresses: ArrayList<Address>) {
        this.user_id = i_user_id
        this.created_at = i_created_at
        this.birth_date = i_birth_date
        this.first_name = i_first_name
        this.last_name = i_last_name
        this.phones = i_phones
        this.thumb = i_thumb
        this.photo = i_photo
        this.addresses = i_addresses
    }
}