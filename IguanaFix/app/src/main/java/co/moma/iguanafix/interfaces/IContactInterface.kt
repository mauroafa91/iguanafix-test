package co.moma.iguanafix.interfaces

import co.moma.iguanafix.model.Contact
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import kotlin.collections.ArrayList

interface IContactInterface {
    @get:GET("contacts")
    val contacts:Observable<ArrayList<Contact>>

    @GET("contacts/{id}")
    fun getContact(@Path("id") id: Int):Observable<Contact>
}