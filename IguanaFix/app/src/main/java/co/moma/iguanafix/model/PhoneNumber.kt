package co.moma.iguanafix.model

import java.io.Serializable

class PhoneNumber: Serializable {
    var type: String = ""
    var number: String = ""

    constructor()

    constructor(i_type: String, i_number: String) {
        this.type = i_type
        this.number = i_number
    }
}