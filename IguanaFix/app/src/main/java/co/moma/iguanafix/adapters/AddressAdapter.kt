package co.moma.iguanafix.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import co.moma.iguanafix.R
import co.moma.iguanafix.model.Address

class AddressAdapter(private var addressList: ArrayList<Address>) : RecyclerView.Adapter<AddressAdapter.ViewHolder>() {

    private var TAG = "PhoneNumberAdapter"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvWorkAddress.text = setTextViewContent(addressList[position].home)
        holder.tvHomeAddress.text = setTextViewContent(addressList[position].work)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.model_contact_address, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return addressList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvWorkAddress = itemView.findViewById<TextView>(R.id.tv_work_address)
        val tvHomeAddress = itemView.findViewById<TextView>(R.id.tv_home_address)
    }

    fun setTextViewContent(content: String) : String{

        var result = "Not defined"

        if(!content.isNullOrEmpty()) {
            result = content
        }

        return result
    }
}