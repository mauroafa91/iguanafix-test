package co.moma.iguanafix.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.moma.iguanafix.R
import co.moma.iguanafix.model.Contact
import co.moma.iguanafix.model.PhoneNumber

class PhoneNumberAdapter(private var phoneNumberList: ArrayList<PhoneNumber>) : RecyclerView.Adapter<PhoneNumberAdapter.ViewHolder>() {

    private var TAG = "PhoneNumberAdapter"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvPhoneNumberType.text = setTextViewContent(phoneNumberList[position].type)
        holder.tvPhoneNumber.text = setTextViewContent(phoneNumberList[position].number)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneNumberAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.model_phone_number, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return phoneNumberList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvPhoneNumberType = itemView.findViewById<TextView>(R.id.tv_phone_number_type)
        val tvPhoneNumber = itemView.findViewById<TextView>(R.id.tv_phone_number)
    }

    fun setTextViewContent(content: String?) : String{

        var result = "Not defined"

        if(!content.isNullOrEmpty()) {
            result = content!!
        }

        return result
    }
}