package co.moma.iguanafix.Activities

import android.app.SearchManager
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import co.moma.iguanafix.R
import co.moma.iguanafix.adapters.ContactListAdapter
import co.moma.iguanafix.interfaces.IContactInterface
import co.moma.iguanafix.model.Contact
import co.moma.iguanafix.model.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.vertical_rv.*

class Main : AppCompatActivity(), SearchView.OnQueryTextListener {

    internal lateinit var contactAPI: IContactInterface
    internal lateinit var compositeDisposable: CompositeDisposable
    private var contactAdapter: ContactListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vertical_rv)

        val retroFit = RetrofitClient.instance
        contactAPI = retroFit.create(IContactInterface::class.java)
        compositeDisposable = CompositeDisposable()

        rv_vertical.layoutManager = LinearLayoutManager(this)
        rv_vertical.setHasFixedSize(true)
        rv_vertical.adapter = ContactListAdapter(ArrayList())
        fastscroll.setRecyclerView(rv_vertical)

        getContactList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val mItem = menu!!.findItem(R.id.action_search) as MenuItem
        val searchView: SearchView = mItem.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(true)
        searchView.setOnQueryTextListener(this)

        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if(newText != null) {
            contactAdapter!!.filterContacts(newText)
        }
        return true
    }

    fun getContactList() {
        compositeDisposable.add(contactAPI.contacts
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{contacts->displayData(contacts)}
        )
    }

    fun displayData(contacts: ArrayList<Contact>?) {
        contacts!!.sortBy{ it -> it.first_name }
        contactAdapter = ContactListAdapter(contacts)
        rv_vertical.adapter = contactAdapter
        fastscroll.setRecyclerView(rv_vertical)
        rv_vertical.adapter.notifyDataSetChanged()
    }
}
