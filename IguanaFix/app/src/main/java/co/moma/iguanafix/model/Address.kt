package co.moma.iguanafix.model

import java.io.Serializable

class Address: Serializable {
    var home: String = ""
    var work: String = ""

    constructor()

    constructor(i_home: String, i_work: String) {
        this.home = i_home
        this.work = i_work
    }
}